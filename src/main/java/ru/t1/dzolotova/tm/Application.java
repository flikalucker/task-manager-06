package ru.t1.dzolotova.tm;

import ru.t1.dzolotova.tm.constant.ArgumentConst;
import ru.t1.dzolotova.tm.constant.CommandConst;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:\n");
            final String command = scanner.nextLine();
            processCommand(command);
        }
   }

    private static void processArguments(final String[] arg){
        if(arg == null || arg.length == 0) return;
        for (int i = 0; i < arg.length; i++) {
            runArgument(arg[i]);
        }
        System.exit(0);
    }

    private static void processCommand(final String param){
        switch (param) {
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
        }
    }

    private static void runArgument(final String param){
        switch (param) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showArgumentError();
        }
    }

    private static void showWelcome(){
        System.out.println("** WELCOME TO TASK MANAGER**");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Diana Zolotova");
        System.out.println("email: dzolotova@t1-consulting.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showHelp() {
        System.out.printf("%s, %s : Display developer info. \n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s : Display program version. \n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s : Display list of terminal commands. \n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s : Close application. \n", CommandConst.EXIT);
    }

    private static void showArgumentError(){
        System.out.println("[ERROR]");
        System.out.println("This argument is not supported");
        System.exit(1);
    }

    private static void showCommandError(){
        System.out.println("[ERROR]");
        System.out.println("This command is not supported");
        System.exit(1);
    }

    public static void exit() {
        System.exit(0);
    }

}